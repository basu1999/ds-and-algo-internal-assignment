#include <iostream>
using namespace std;

class Node {
public:
    int val;
    Node * next;
    Node(int v)
    {
        val=v;
        next=nullptr;
    }

    void insert(Node * head,int val);
    void  Delete(Node **head); //deletes the first node;

};

void traverse(Node *head)
{
    while(head!=nullptr)
    {
        cout<< (*head).val<< " ";
        head=(*head).next;
    }
    cout<<endl;
}
int search(Node *head, int val)
{
    int pos=0;

    while(head!=nullptr)
    {
        if((*head).val==val)
            return pos;
        head=(*head).next;
        ++pos;
    }
    return -1;

}
void Node::insert(Node * head,int val)
{
    Node *toBe=new Node(val);
    Node *temp=head;
    while((*temp).next!=nullptr)
    {
        temp=(*temp).next;
    }
    (*temp).next=toBe;

}

void Node::Delete(Node **head)
{
    Node *temp=*head;
    if(temp!=nullptr)
    {
        *head=temp->next;
        //delete temp;
        delete temp;
    }
}


int main()
{
    Node * first=new Node(5);
    Node *head=first;
    (*first).insert(head,6);
    (*first).insert(head,7);
    (*first).insert(head,8);
    (*first).Delete(&head);
    cout<<"Traversal: ";
    traverse(head);
    int val=8;
    int s=search(head,val);
    if (s==-1)
        cout<<"Value not found!"<<endl;
    else
        cout<<"Value found! "<<endl<<"Position: "<<s<<endl;

    
    return 0;
}



