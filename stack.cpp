#include <iostream>
using namespace std;

void Push(int *A,int val);
int Pop(int *A);
bool isStackFull(int top);
bool isStackEmpty(int top);

int top=0;
const int size=100;
int main()
{
    int array[size],val;
    cout<<"Enter the value to enter: ";
    cin>>val;
    Push(array,val);
    Push(array,6);
    Push(array,7);
    cout<<"Pop: "<<Pop(array)<<endl;
    return 0;
}


bool isStackFull(int top)
{
    if(top==size)
        return true;
    return false;
}

bool isStackEmpty(int top)
{
    if(top==0)
        return true;
    return false;
}


void Push(int *A,int val)
{
    if (isStackFull(top))
    {
        cerr<<"Error! Stack full"<<endl;
        exit(-1);
    }

    top+=1;
    A[top]=val;
}


int Pop(int *A)
{
    if(isStackEmpty(top))
    {
        cerr<<"Error! Stack empty"<<endl;
        exit(-1);
    }
    int temp=A[top];
    --top;
    return temp;
}

