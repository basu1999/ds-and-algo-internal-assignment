#include <iostream>
using namespace std;


void quick_sort(int *A,int s,int e);
int partition(int *A,int l,int r);
void swap(int *a,int *b);

int main()
{
    int size=5,array[size];
    cout<<"Enter the elements: ";
    for(int i=0;i<size;i++)
        cin>>array[i];
    quick_sort(array,0,size-1);
    cout<<"The sorted array: ";
    for(int i=0;i<size;i++)
        cout<<array[i]<<" ";
    cout<<endl;
    return 0;
}


void quick_sort(int *A,int s,int e)
{
    if(s<e)
    {
        int q=partition(A,s,e);
        quick_sort(A,s,q-1);
        quick_sort(A,q+1,e);
    }
}


int partition(int *A,int l,int r)
{
    int x=A[r];
    int i=l-1;
    for(int j=l;j<r;j++)
    {
        if(A[j]<x)
        {
            i+=1;
            swap(&A[j],&A[i]);
        }
    }
    swap(A[i+1],A[r]);
    return i;

}

void swap(int *a,int *b)
{
    int temp=*b;
    *b=*a;
    *a=temp;
}





