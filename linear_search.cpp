#include <iostream>
using namespace std;

int linear_search(int *A,int s,int v);

int main()
{
    int size=5,array[size];
    cout<<"Enter the elements: ";
    for(int i=0;i<size;i++)
        cin>>array[i];
    int val;
    cout<<"Enter the value to search: ";
    cin>>val;
    int pos=linear_search(array,size,val);
    if(pos==-1)
        cout<<"Value not found!"<<endl;
    else
        cout<<"Value found!"<<endl<<"Position: "<<pos<<endl;
    return 0;
}


int linear_search(int *A,int s,int v)
{
    for(int i=0;i<s;i++)
        if(A[i]==v)
            return i;
    return -1;
}


       

