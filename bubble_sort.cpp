#include <iostream>
using namespace std;

void Bubble_Sort(int *A,int n);
void swap(int *a,int *b);

int main()
{
    int size=5;
    int array[size];
    cout<<"Enter the elements: ";
    for(int i=0;i<size;i++)
        cin>>array[i];
    Bubble_Sort(array,size);
    cout<<"Sorted array: ";
    for(int i=0;i<size;i++)
        cout<<array[i]<<" ";
    cout<<endl;
    return 0;
}

void Bubble_Sort(int *A,int n)
{
    for(int i=0;i<n-1;i++)
        for(int j=0;j<n-i-1;j++)
            if (A[j]>A[j+1])
                swap(&A[j],&A[j+1]);
}


void swap(int *a,int *b)
{
    int temp=*b;
    *b=*a;
    *a=temp;
}


