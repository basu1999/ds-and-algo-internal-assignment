#include <iostream>
using namespace std;

void merge_sort(int *A,int s,int e);
void merge(int *A,int s,int m,int e);

int main()
{
    int size=5;
    int array[size];
    cout<<"Enter the elements: ";
    for(int i=0;i<size;i++)
        cin>>array[i];
    merge_sort(array,0,size-1);
    cout<<"Sorted array: ";
    for(int i=0;i<size;i++)
        cout<<array[i]<<" ";
    cout<<endl;
    return 0;
}


void merge_sort(int *A,int s,int e)
{
    if(s<e)
    {
        int q=s+(e-s)/2;
        merge_sort(A,s,q);
        merge_sort(A,q+1,e);
        merge(A,s,q,e);
    }
}


void merge(int *A,int s,int m,int e)
{
    int l1=m-s+1,l2=e-m;
    int lArray[l1],rArray[l2];
    for(int i=0;i<l1;i++)
        lArray[i]=A[s+i];
    for(int j=0;j<l2;j++)
        rArray[j]=A[m+1+j];

    int i=0,j=0,k=s;
    while(i<l1 && j<l2)
    {
        if (lArray[i]<rArray[j])
        {
            A[k]=lArray[i];
            i++;k++;
        }
        else
        {
            A[k]=rArray[j];
            j++;k++;
        }
    }
    while(i<l1)
    { 
        A[k]=lArray[i];
        k++;i++;
    }
    while(j<l2)
    {
        A[k]=rArray[j];
        j++;k++;
    }
}




    

