#include <iostream>
using namespace std;
const int size=5;
void Insertion_Sort(int *a,int n);

int main()
{
    int array[size];
    cout<<"Enter the elements: ";
    for(int i=0;i<size;i++)
        cin>>array[i];
    Insertion_Sort(array,size);
    printf("Sorted array: ");
    for(int i=0;i<size;i++)
        cout<<array[i]<<" ";
    cout<<endl;
    return 0;
}

void Insertion_Sort(int *A,int n)
{
    int i,key;
    for(int j=1;j<n;j++)
    {
        key=A[j];
        i=j-1;
        while(i>=0 && A[i]>key)
        {
            A[i+1]=A[i];
            i=i-1;
        }
        A[i+1]=key;
    }
}

