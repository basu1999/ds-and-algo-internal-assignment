#include <iostream>
using namespace std;

void Selection_Sort(int *A,int n);
void swap(int *a,int *b);
int main()
{
    int size=5;
    int array[size];
    cout<<"Enter the elements: ";
    for(int i=0;i<size;i++)
        cin>>array[i];
    Selection_Sort(array,size);
    cout<<"Sorted array: ";
    for(int i=0;i<size;i++)
        cout<<array[i]<<" ";
    cout<<endl;
    return 0;
}


void Selection_Sort(int *A,int n)
{
    int min_idx;
    for(int i=0;i<n;i++)
    {
        min_idx=i;
        for(int j=i;j<n;j++)
        {
            if(A[j]<A[min_idx])
                min_idx=j;
        }
        swap(&A[i],&A[min_idx]);
    }
}

void swap(int *a,int *b)
{
    int temp=*b;
    *b=*a;
    *a=temp;
}

