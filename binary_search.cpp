#include <iostream>
using namespace std;
int binary_search(int *A,int s,int e,int v);

int main()
{
    int size=5,array[size];
    cout<<"Enter the elements: ";
    for(int i=0;i<size;i++)
        cin>>array[i];
    cout<<"Enter the element to search: ";
    int val;
    cin>>val;
    int pos=binary_search(array,0,size,val);
    if(pos ==-1)
        cout<<"Element not found!"<<endl;
    else
        cout<<"Element found!"<<endl<<"Position: "<<pos<<endl;
    return 0;
}


int binary_search(int *A,int s,int e,int v)
{
    if (s>e)
        return -1;
    int m=(s+e)/2;
    if(A[m]==v)
        return m;
    else if(A[m]<v)
        return binary_search(A,m+1,e,v);
    else
        return binary_search(A,e,m-1,v);
}
    


